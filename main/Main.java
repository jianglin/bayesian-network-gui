package graph.main;

import graph.panel.ScrollPanel;
import graph.panel.ToolBar;
import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author jianglin
 */
public class Main {

    public static ScrollPanel mainSP;
    public static boolean exitOnClose = true;

    public static void initComponent() {

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (InstantiationException ex) {
	    ex.printStackTrace();
	} catch (ClassNotFoundException ex) {
	    ex.printStackTrace();
	} catch (UnsupportedLookAndFeelException ex) {
	    ex.printStackTrace();
	} catch (IllegalAccessException ex) {
	    ex.printStackTrace();
	}

        JFrame f = new JFrame("URLearning");
        if (exitOnClose) {
            f.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        } else {
            f.setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
        }


        ToolBar tb = new ToolBar(f);
        mainSP = new ScrollPanel();

        Container contentPane = f.getContentPane();
        contentPane.add(tb, BorderLayout.NORTH);
        contentPane.add(mainSP, BorderLayout.CENTER);

        f.setSize(800, 800);
        f.pack();
        f.setVisible(true);

    }

    public static void main(String[] args) {
        initComponent();

    }
}
