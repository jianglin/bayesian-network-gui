package graph.component.table;

import datastructures.BayesianNetwork;
import java.awt.*;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.table.*;

/**
 * A class to represent CPT table which will be placed on the CPT component.
 *
 * @author jianglin
 */
public class Table extends JPanel {

    /**
     * number of parents of current node
     */
    int numOfParents;
    /**
     * Cardinality of Variable (Node) idx
     */
    int VidxCardinality;
    /**
     * max number of columns of the table (Total columns of the table - row
     * header)
     */
    int maxColumn;
    /**
     * width and height of the table
     */
    int width, height;
    /**
     * Cardinalities of each parents of current node
     */
    int[] parentsCardinalities;

    /**
     * Get Dimension of the table
     *
     * @return Dimension of the table
     */
    public Dimension getDimension() {
        return new Dimension(width, height);
    }

    /**
     * Get ordered Parameters of the table parameters stored in the network and
     * displayed on the table are in different order Therefore, parameters
     * stored in the network need to be sorted
     *
     * @return ordered Parameters of the table
     */
    private double[][] getOrderedParameters(double[][] originalorder) {
        double[][] neworder = null;
        for (int p = 0; p < numOfParents; p++) {
            neworder = new double[originalorder.length / parentsCardinalities[p]][originalorder[0].length * parentsCardinalities[p]];
            for (int i = 0; i < originalorder.length / parentsCardinalities[p]; i++) {
                System.arraycopy(originalorder[i], 0, neworder[i], 0, originalorder[0].length);
            }
            for (int n = 1; n < parentsCardinalities[p]; n++) {
                for (int i = 0; i < originalorder.length / parentsCardinalities[p]; i++) {
                    for (int j = 0; j < originalorder[0].length; j++) {
                        neworder[i][originalorder[0].length * n + j] = originalorder[originalorder.length / parentsCardinalities[p] * n + i][j];
                    }
                }
            }
            originalorder = neworder;
        }
        return neworder;
    }

    public Table() {
        numOfParents = -1;
        VidxCardinality = -1;
        maxColumn = -1;
        width = 0;
        height = 0;
    }

    /**
     * This constructor gives a CPT component that knows following parameters.
     *
     * @param idx id of the table
     * @param Network network that stores information to be retrieved
     */
    public Table(final int idx, final BayesianNetwork network) {

        numOfParents = network.get(idx).getParentOrder().size();
        VidxCardinality = network.get(idx).getCardinality();
        if (numOfParents > 0) {
            parentsCardinalities = new int[numOfParents];
            /**
             * headers that need to be displayed on the table
             */
            ArrayList<ColumnGroup>[] headerlevels = new ArrayList[numOfParents];
            maxColumn = 1;
            ColumnGroup c0;
            for (int i = 0; i < numOfParents; i++) {
                headerlevels[i] = new ArrayList<ColumnGroup>();
                parentsCardinalities[i] = network.get(network.get(idx).getParentOrder().get(i)).getCardinality();
                for (int j = 0; j < maxColumn; j++) {
                    for (int k = 0; k < network.get(network.get(idx).getParentOrder().get(i)).getCardinality(); k++) {
                        c0 = new ColumnGroup(network.get(network.get(idx).getParentOrder().get(i)).getValueString(k));
                        headerlevels[i].add(c0);
                    }
                }
                maxColumn *= network.get(network.get(idx).getParentOrder().get(i)).getCardinality();
            }

            /**
             * last row of column headers
             */
            Object[] lastColheaders = new Object[maxColumn + 1];
            lastColheaders[0] = network.get(network.get(idx).getParentOrder().get(numOfParents - 1)).getName();
            int cardinality = network.get(network.get(idx).getParentOrder().get(numOfParents - 1)).getCardinality();

            for (int i = 1; i < maxColumn + 1; i++) {
                lastColheaders[i] = network.get(network.get(idx).getParentOrder().get(numOfParents - 1)).getValueString((i - 1) % cardinality);
            }

            double ordered[][] = getOrderedParameters(network.get(idx).getParameters());
            Object[][] prob = new Object[VidxCardinality][maxColumn + 1];
            int k;
            for (int i = 0; i < VidxCardinality; i++) {
                prob[i][0] = network.get(idx).getValueString(i);
                k = i;
                for (int j = 1; j < maxColumn + 1; j++) {
                    prob[i % VidxCardinality][j] = ordered[0][k];
                    k += VidxCardinality;
                }
            }

            DefaultTableModel dm = new DefaultTableModel() {
                @Override
                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return columnIndex != 0;
                }
            };
            dm.setDataVector(prob, lastColheaders);


            JTable table = new JTable(dm) {
                @Override
                protected JTableHeader createDefaultTableHeader() {
                    GroupableTableHeader gth = new GroupableTableHeader(columnModel);
                    gth.setLevel(network.get(idx).getParentOrder().size());
                    return gth;
                }
            };


            TableColumnModel cm = table.getColumnModel();

            if (numOfParents > 1) {
                ColumnGroup[] firstColumn = new ColumnGroup[numOfParents - 1];
                for (int i = 0; i < numOfParents - 1; i++) {
                    c0 = new ColumnGroup(network.get(network.get(idx).getParentOrder().get(i)).getName());
                    firstColumn[i] = c0;
                }

                firstColumn[numOfParents - 2].add(cm.getColumn(0));
                if (numOfParents > 2) {
                    for (int i = numOfParents - 3; i >= 0; i--) {
                        firstColumn[i].add(firstColumn[i + 1]);
                    }
                }
                int l = numOfParents - 2;
                int groupsize = network.get(network.get(idx).getParentOrder().get(l + 1)).getCardinality();
                for (int n = 0; n < headerlevels[l + 1].size(); n++) {
                    headerlevels[l].get(n / groupsize).add(cm.getColumn(n + 1));
                }
                //group other rows
                if (numOfParents > 2) {
                    for (l = numOfParents - 3; l >= 0; l--) {
                        groupsize = network.get(network.get(idx).getParentOrder().get(l + 1)).getCardinality();
                        for (int n = 0; n < headerlevels[l + 1].size(); n++) {
                            headerlevels[l].get(n / groupsize).add(headerlevels[l + 1].get(n));
                        }
                    }

                }

                //add first column header to table
                GroupableTableHeader header = (GroupableTableHeader) table.getTableHeader();
                header.addColumnGroup(firstColumn[0]);
                for (int i = 0; i < headerlevels[0].size(); i++) {
                    header.addColumnGroup(headerlevels[0].get(i));
                }
            }

            JScrollPane scroll = new JScrollPane(table);

            height = 20 * (numOfParents + VidxCardinality);
            width = 50 * maxColumn;
            scroll.setPreferredSize(new Dimension(width, height));
            add(scroll);
            setPreferredSize(new Dimension(width, height));
        } else if (numOfParents == 0) {

            Object prob[][] = new Object[VidxCardinality][2];
            for (int i = 0; i < VidxCardinality; i++) {
                prob[i][0] = network.get(idx).getValueString(i);
                prob[i][1] = network.get(idx).getParameters()[0][i];
            }
            Object header[] = {"", ""};
            JTable table = new JTable(prob, header);
            table.setTableHeader(null);
            JScrollPane scroll = new JScrollPane(table);
            height = 20 * VidxCardinality;
            width = 50 * 2;
            scroll.setPreferredSize(new Dimension(width, height));
            add(scroll);

        }



    }
}
