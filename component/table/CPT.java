package graph.component.table;

import datastructures.BayesianNetwork;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JComponent;
import graph.panel.GraphPanel;
import java.awt.Graphics;

/**
 * A class to represent CPT component.
 *
 * @author jianglin
 */
public class CPT extends JComponent {

    /**
     * mouse coordinates on the screen
     */
    volatile int screenX = 0;
    volatile int screenY = 0;
    /**
     * CPT component coordinates
     */
    volatile int cptX = 0;
    volatile int cptY = 0;
    /**
     * The GraphPanel where INFO component lays
     */
    GraphPanel gp;
    /**
     * The BayesianNetwork which stores CPT information
     */
    BayesianNetwork network;
    /**
     * unique id for current CPT component
     */
    int id;
    /**
     * node size determines size of the CPTcomponent
     */
    int nodeSize;
    /**
     * width of current CPT component
     */
    int width;
    /**
     * visibility state of the component
     */
    boolean isVisible;

    /**
     * Get visibility states of current CPT component
     *
     * @return the visible states, true or false
     */
    public boolean getCPTVisible() {
        return isVisible;
    }

    /**
     * Set visible states for current CPT component
     *
     * @param visible visible state that needs to be set, true or false
     */
    public void setCPTVisible(boolean visible) {
        isVisible = visible;
    }

    public CPT() {
        gp = null;
        network = null;
        id = -1;
        width = 0;
        nodeSize = 0;
        isVisible = false;
    }

    /**
     * This constructor gives a CPT component that knows following parameters.
     *
     * @param GP the GraphPanel where INFO component lays
     * @param NodeSize determines component size
     * @param ID current CPT component id
     * @param Network network that stores information to retrieve
     * @param initX initial x coordinate of the CPT component
     * @param initY initial y coordinate of the CPT component
     */
    public CPT(GraphPanel GP, int NodeSize, int ID, BayesianNetwork Network, int initX, int initY) {
        gp = GP;
        network = Network;
        id = ID;
        nodeSize = NodeSize;
        isVisible = false;
        Table t = new Table(id, network);
        t.setSize(t.getDimension());
        t.setLocation(0, 10);
        add(t);

        width = t.getWidth();
        setBounds(initX + nodeSize * 4 / 5, initY + nodeSize * 4 / 5, width, t.getHeight() + 10);
        setVisible(isVisible);

        addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {

                screenX = e.getXOnScreen();
                screenY = e.getYOnScreen();

                cptX = getX();
                cptY = getY();

            }

            @Override
            public void mouseReleased(MouseEvent e) {
                cptX = getX();
                cptY = getY();
                gp.redrawCPT();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {
                int deltaX = e.getXOnScreen() - screenX;
                int deltaY = e.getYOnScreen() - screenY;
                setLocation(cptX + deltaX, cptY + deltaY);
                gp.redrawCPT();
            }

            @Override
            public void mouseMoved(MouseEvent e) {
            }
        });

    }

    /**
     * add a rectangle bar on the top of the component for dragging
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.GRAY);
        g.fillRect(0, 0, width, 20);
    }
};
