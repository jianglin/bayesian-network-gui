package graph.component.info;

import datastructures.BayesianNetwork;
import graph.panel.GraphPanel;
import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.swing.*;

/**
 * A class to represent information table on INFO panel.
 *
 * @author jianglin
 */
public class InfoTable extends JPanel {

    /**
     * The GraphPanel where INFO panel lays
     */
    GraphPanel gp;
    /**
     * node size determines size of table
     */
    int nodeSize;
    /**
     * unique id for current info table
     */
    int id;
    /**
     * table width and height
     */
    int width, height;
    /**
     * The BayesianNetwork which has stored information
     */
    BayesianNetwork network;

    /**
     * Get Dimension of the infotable
     *
     * @return Dimension of the InfoTable
     */
    public Dimension getDimension() {
        return new Dimension(width, height);
    }

    public InfoTable() {
        gp = null;
        nodeSize = 0;
        id = -1;
        width = 0;
        height = 0;
    }

    /**
     * This constructor gives a InfoTable that knows following parameters.
     *
     * @param GP the GraphPanel where INFO panel lays
     * @param NodeSize determines nodes and panels size
     * @param ID current INFO panel id
     * @param Network network that stores information to retrieve
     */
    public InfoTable(GraphPanel GP, int NodeSize, int ID, BayesianNetwork Network) {
        gp = GP;
        nodeSize = NodeSize;
        id = ID;
        network = Network;
        String[] columnNames = {"Property", "Value"};
        String parents = "";
        String children = "";
        String values = "";
        //parents information
        if (network.get(id).getParentOrder().size() > 0) {
            for (int i = 0; i < network.get(id).getParentOrder().size() - 1; i++) {
                parents += network.get(network.get(id).getParentOrder().get(i)).getName() + ", ";
            }
            parents += network.get(network.get(id).getParentOrder().get(network.get(id).getParentOrder().size() - 1)).getName();
        } else {
            parents = "No Parent";
        }
        //children information
        List<Integer> childrenList = new ArrayList<Integer>();
        for (int i = 0; i < network.size(); i++) {
            for (int j = 0; j < network.get(i).getParentOrder().size(); j++) {
                if (network.get(i).getParentOrder().get(j) == id) {
                    childrenList.add(i);
                }
            }
        }
        Set<Integer> childrenSet = new LinkedHashSet<Integer>(childrenList);
        System.out.println(" childrenSet" + childrenSet);
        if (childrenSet.size() > 0) {
            Iterator iter = childrenSet.iterator();
            while (iter.hasNext()) {
                children += network.get((Integer) iter.next()).getName() + ", ";
            }
            children = children.substring(0, children.length() - 2);
        } else {
            children = "No Child";
        }
        //possible values for this node
        for (int i = 0; i < network.getCardinality(id) - 1; i++) {
            values += network.get(id).getValueString(i) + ", ";
        }
        values += network.get(id).getValueString(network.getCardinality(id) - 1);

        Object[][] data = {
            {"Node Name", network.get(id).getName()},
            {"Node ID", new Integer(id)},
            {"Cardinality", new Integer(network.getCardinality(id))},
            {"Values", values},
            {"Parents", parents},
            {"Children", children}
        };

        final JTable table = new JTable(data, columnNames);
        width = nodeSize * 2;
        height = nodeSize * 2;
        table.setPreferredScrollableViewportSize(new Dimension(width, height));
        table.setFillsViewportHeight(true);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        JScrollPane jsp = new JScrollPane(table);
        jsp.setPreferredSize(new Dimension(width, height));
        add(jsp);
    }
}
