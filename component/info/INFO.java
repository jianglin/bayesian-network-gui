package graph.component.info;

import datastructures.BayesianNetwork;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JComponent;
import graph.panel.GraphPanel;
import java.awt.Graphics;

/**
 * A class to represent node information component.
 *
 * @author jianglin
 */
public class INFO extends JComponent {

    /**
     * mouse coordinates on the screen
     */
    volatile int screenX = 0;
    volatile int screenY = 0;
    /**
     * INFO component coordinates
     */
    volatile int infoX = 0;
    volatile int infoY = 0;
    /**
     * The GraphPanel where INFO component lays
     */
    GraphPanel gp;
    /**
     * The BayesianNetwork which has stored information
     */
    BayesianNetwork network;
    /**
     * unique id for current INFO component
     */
    int id;
    /**
     * width of current INFO component
     */
    int width;
    /**
     * node size determines size of the node and component
     */
    int nodeSize;
    /**
     * visibility state of the component
     */
    boolean isVisible;

    /**
     * Get visible states of current INFO component
     *
     * @return the visible states, true or false
     */
    public boolean getINFOVisible() {
        return isVisible;
    }

    /**
     * Set visible states for current INFO component
     *
     * @param visible visible state that needs to be set, true or false
     */
    public void setINFOVisible(boolean visible) {
        isVisible = visible;
    }

    public INFO() {
        gp = null;
        network = null;
        id = -1;
        width = 0;
        nodeSize = 0;
        isVisible = false;
    }

    /**
     * This constructor gives a INFO component that knows following parameters.
     *
     * @param GP the GraphPanel where INFO component lays
     * @param NodeSize determines nodes and component size
     * @param ID current INFO component id
     * @param Network network that stores information to retrieve
     * @param initX initial x coordinate of the INFO component
     * @param initY initial y coordinate of the INFO component
     */
    public INFO(GraphPanel GP, int NodeSize, int ID, BayesianNetwork Network, int initX, int initY) {
        gp = GP;
        network = Network;
        id = ID;
        nodeSize = NodeSize;
        isVisible = false;
        InfoTable t = new InfoTable(gp, nodeSize, id, network);
        t.setSize(t.getDimension());
        t.setLocation(0, 10);
        add(t);

        width = t.getWidth();
        setBounds(initX + nodeSize / 5 - width, initY + nodeSize * 4 / 5 - 8, t.getWidth(), t.getHeight() + 10);

        setVisible(isVisible);

        addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
                screenX = e.getXOnScreen();
                screenY = e.getYOnScreen();
                infoX = getX();
                infoY = getY();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                infoX = getX();
                infoY = getY();
                gp.redrawCPT();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {
                int deltaX = e.getXOnScreen() - screenX;
                int deltaY = e.getYOnScreen() - screenY;
                setLocation(infoX + deltaX, infoY + deltaY);
                gp.redrawCPT();
            }

            @Override
            public void mouseMoved(MouseEvent e) {
            }
        });

    }

    /**
     * add a rectangle bar on the top of the component for dragging
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(0, 0, width, 20);
    }
};
