package graph.component;

import graph.component.node.Node;

/**
 * A class to represent edge between two nodes.
 *
 * @author jianglin
 */
public class Edge {

    /**
     * from Node, source Node
     */
    Node from;
    /**
     * to Node, destination Node
     */
    Node to;

    /**
     * Get source Node of the Edge
     *
     * @return source Node
     */
    public Node getFrom() {
        return from;
    }

    /**
     * Get destination Node of the Edge
     *
     * @return destination Node
     */
    public Node getTo() {
        return to;
    }

    public Edge() {
        from = null;
        to = null;
    }

    /**
     * This constructor gives a Edge that knows source and destination.
     *
     * @param source source Node
     * @param destination destination Node
     */
    public Edge(Node source, Node destination) {
        from = source;
        to = destination;
    }
}