package graph.component.node;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import javax.swing.JButton;
import graph.component.table.CPT;
import graph.panel.GraphPanel;

/**
 * A class to represent Button which controls visibility of the CPT.
 *
 * @author jianglin
 */
class CPTButton extends JButton {

    /**
     * mouse coordinates on the screen
     */
    volatile int screenX = 0;
    volatile int screenY = 0;
    /**
     * button coordinates
     */
    volatile int cptButtonX = 0;
    volatile int cptButtonY = 0;
    /**
     * The GraphPanel where cptButton lays
     */
    GraphPanel gp;
    /**
     * HashMap that maps each button to its CPT
     */
    HashMap cptmap;
    /**
     * unique id for current INFO panel
     */
    int id;

    public CPTButton() {
        gp = null;
        cptmap = null;
        id = -1;
    }

    /**
     * This constructor gives a CPTButton that knows following parameters.
     *
     * @param GP the GraphPanel where INFO panel lays
     * @param CPTMap HashMap that maps button to its CPT
     * @param ID ID of current CPTButton
     */
    public CPTButton(GraphPanel GP, HashMap CPTMap, int ID) {
        gp = GP;
        cptmap = CPTMap;
        id = ID;

        addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (!((CPT) (cptmap.get(id))).getCPTVisible()) {
                    ((CPT) (cptmap.get(id))).setVisible(true);
                    ((CPT) (cptmap.get(id))).setCPTVisible(true);
                    gp.redrawCPT();
                } else {
                    ((CPT) (cptmap.get(id))).setVisible(false);
                    ((CPT) (cptmap.get(id))).setCPTVisible(false);
                    gp.redrawCPT();
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
                screenX = e.getXOnScreen();
                screenY = e.getYOnScreen();
                cptButtonX = getX();
                cptButtonY = getY();

            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
    }
}