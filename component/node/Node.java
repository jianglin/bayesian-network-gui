package graph.component.node;

import graph.component.info.INFO;
import java.awt.*;
import java.awt.event.*;
import java.util.HashMap;
import javax.swing.*;
import graph.panel.GraphPanel;
import graph.component.table.CPT;

/**
 * A class to represent Node.
 *
 * @author jianglin
 */
public class Node extends JComponent {

    /**
     * mouse coordinates on the screen
     */
    volatile int screenX = 0;
    volatile int screenY = 0;
    /**
     * node coordinates
     */
    volatile int nodeX = 0;
    volatile int nodeY = 0;
    /**
     * temp coordinates for dragging. when dragging a node, stored node location
     * hasn't change yet, tempX and tempY record temporary location. All the
     * paint method draw graph based on temp location
     *
     */
    volatile int tempX = 0;
    volatile int tempY = 0;
    /**
     * The GraphPanel where Node lays
     */
    GraphPanel gp;
    /**
     * HashMap that maps each node to its CPT
     */
    HashMap cptmap;
    /**
     * HashMap that maps each node to its INFO panel
     */
    HashMap infomap;
    /**
     * size of the node
     */
    int nodeSize;
    /**
     * node name
     */
    String variableName;
    /**
     * node id
     */
    int id;

    /**
     * Get temp x coordinate
     *
     * @return temp x coordinate
     */
    public int getTempX() {
        return tempX;
    }

    /**
     * Get temp y coordinate
     *
     * @return temp y coordinate
     */
    public int getTempY() {
        return tempY;
    }

    /**
     * Set temp x coordinate
     *
     * @param x x coordinate that needs to be set
     */
    public void setTempX(int x) {
        tempX = x;
    }

    /**
     * Set temp y coordinate
     *
     * @param y y coordinate that needs to be set
     */
    public void setTempY(int y) {
        tempY = y;
    }

    /**
     * Get x coordinate of the node
     *
     * @return node x coordinate
     */
    public int getNodeX() {
        return nodeX;
    }

    /**
     * Get y coordinate of the node
     *
     * @return node y coordinate
     */
    public int getNodeY() {
        return nodeY;
    }

    /**
     * Set node x coordinate
     *
     * @param x x coordinate that needs to be set
     */
    public void setNodeX(int x) {
        nodeX = x;
    }

    /**
     * Set node y coordinate
     *
     * @param y y coordinate that needs to be set
     */
    public void setNodeY(int y) {
        nodeY = y;
    }

    /**
     * Get id of the node
     *
     * @return node id
     */
    public int getID() {
        return id;
    }

    public Node() {
        gp = null;
        cptmap = null;
        infomap = null;
        nodeSize = 0;
        variableName = "";
    }

    /**
     * This constructor gives a Node that knows following parameters.
     *
     * @param GP the GraphPanel where Node lays
     * @param CPTMap HashMap maps node to its CPT
     * @param InfoMap HashMap maps node to its INFO panel
     * @param NodeSize determines nodes size
     * @param initX initial x coordinate of the Node
     * @param initY initial y coordinate of the Node
     */
    public Node(GraphPanel GP, HashMap CPTMap, HashMap InfoMap, int ID, int NodeSize, String VariableName, int initX, int initY) {
        gp = GP;
        id = ID;
        variableName = VariableName;
        cptmap = CPTMap;
        infomap = InfoMap;
        nodeSize = NodeSize;
        tempX = initX;
        tempY = initY;
        nodeX = initX;
        nodeY = initY;
        setBorder(new RoundedBorder(nodeSize));
        setBackground(Color.WHITE);
        setBounds(initX, initY, nodeSize, nodeSize);
        setOpaque(true);
        JLabel l = new JLabel();
        l.setText(variableName);
        l.setLocation(nodeSize / 8, 0);

        l.setHorizontalAlignment(0);
        l.setSize(nodeSize * 4 / 5, nodeSize);

        CPTButton cb = new CPTButton(gp, cptmap, id);
        cb.setText("T");
        cb.setLocation(nodeSize * 4 / 5, nodeSize * 4 / 5);
        cb.setSize(nodeSize / 5, nodeSize / 5);


        add(l);
        add(cb);

        addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    if (!((INFO) (infomap.get(id))).getINFOVisible()) {
                        ((INFO) (infomap.get(id))).setINFOVisible(true);
                        ((INFO) (infomap.get(id))).setVisible(true);

                        gp.redrawINFO();
                    } else {
                        ((INFO) (infomap.get(id))).setINFOVisible(false);
                        ((INFO) (infomap.get(id))).setVisible(false);

                        gp.redrawINFO();
                    }
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

                screenX = e.getXOnScreen();
                screenY = e.getYOnScreen();

                nodeX = getX();
                nodeY = getY();



            }
            /*
             * Node coordinate changed after dragging
             */

            @Override
            public void mouseReleased(MouseEvent e) {
                gp.updatePosition();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {
                int deltaX = e.getXOnScreen() - screenX;
                int deltaY = e.getYOnScreen() - screenY;
                tempX = nodeX + deltaX;
                tempY = nodeY + deltaY;
                setLocation(tempX, tempY);
                //CPT and INFO componets will follow the Node movement
                ((CPT) (cptmap.get(id))).setLocation(tempX + nodeSize * 4 / 5, tempY + nodeSize * 4 / 5);
                ((INFO) (infomap.get(id))).setLocation(tempX + nodeSize / 5 - 2 * nodeSize, tempY + nodeSize * 4 / 5 - 8);
                gp.redraw();

                //expand GraphPanel  
                if (tempX + nodeSize > gp.getX_MAX()) {
                    gp.setX_MAX(tempX + 5 * nodeSize);
                    gp.setPreferredSize(new Dimension(gp.getX_MAX(), gp.getY_MAX()));
                    gp.revalidate();
                    gp.redraw();
                }
                if (tempY + nodeSize > gp.getY_MAX()) {
                    gp.setY_MAX(tempY + 5 * nodeSize);
                    gp.setPreferredSize(new Dimension(gp.getX_MAX(), gp.getY_MAX()));
                    gp.revalidate();
                    gp.redraw();
                }


            }

            @Override
            public void mouseMoved(MouseEvent e) {
            }
        });



    }
}
