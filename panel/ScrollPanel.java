package graph.panel;

import datastructures.BayesianNetwork;
import java.awt.Dimension;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 * A class to represent ScrollPanel
 *
 * @author jianglin
 */
public class ScrollPanel extends JPanel {

    /**
     * The GraphPanel
     */
    static GraphPanel gp;
    /**
     * The BayesianNetwork that stores information
     */
    BayesianNetwork network;
    /**
     * The JScrollPane
     */
    JScrollPane scroller;

    /**
     * Redraw the graph
     */
    public void redraw() {
        repaint();
    }

    /**
     * Get scroller
     *
     * @return scroller
     */
    public JScrollPane getScrollPane() {
        return scroller;
    }

    public ScrollPanel() {
        gp = new GraphPanel();
        setPreferredSize(new Dimension(600, 600));

    }

    /**
     * This constructor gives a ScrollPanel given a Network.
     *
     * @param Network Network that stores information
     */
    public ScrollPanel(BayesianNetwork Network) {
        gp = new GraphPanel(this);
        network = Network;
        scroller = new JScrollPane(
                gp,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroller.setPreferredSize(new Dimension(700, 700));
        gp.init(network);
        scroller.setWheelScrollingEnabled(true);
        add(scroller);
        scroller.getVerticalScrollBar().setUnitIncrement(16);
        scroller.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {
                repaint();
            }
        });

        scroller.getHorizontalScrollBar().setUnitIncrement(16);
        scroller.getHorizontalScrollBar().addAdjustmentListener(new AdjustmentListener() {
            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {

                repaint();
            }
        });


    }
}
