package graph.panel;

import datastructures.BayesianNetwork;
import graph.component.ComponentMover;
import graph.component.Edge;
import graph.component.info.INFO;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.*;
import graph.component.node.Node;
import graph.component.table.CPT;
import graph.layout.ParentOrderLayout;

/*
 * Note: Some problems.
 * 1. All the redraw() methods need to be modified to improve efficiency.
 * 2. GraphPanel itself is not resizable.
 * 3. I use GraphPanel and ScrollPanel to make the panel scrollable, but when
 *    I write table package later, I found the way to make panel scrollable 
 *    in table package seems much better.
 */


/**
 * A class to represent a panel where graphs drawn on
 *
 * @author jianglin
 */
public class GraphPanel extends JPanel {

    /**
     * ComponentMover for all movable components
     */
    ComponentMover cm;
    /**
     * HashMap that maps node to its CPT
     */
    static HashMap cptMap;
    /**
     * HashMap that maps node to its INFO
     */
    static HashMap infoMap;
    /**
     * HashMap that maps id to node
     */
    HashMap idMap;
    /**
     * ScrollPanel where the GraphPanel lays
     */
    ScrollPanel sp;
    /**
     * max x and y coordinates
     */
    int X_MAX, Y_MAX;
    /**
     * edges of the graph
     */
    ArrayList<Edge> edges;
    /**
     * nodeSize
     */
    private int nodeSize;
    /**
     * The BayesianNetwork which stores CPT information
     */
    BayesianNetwork network;

    /**
     * Add Node to GraphPanel
     *
     * @param n Node id
     * @return Node that has been added
     */
    Node addNode(Node n) {
        //add action listener
        cm.registerComponent(n);
        add(n);
        return n;
    }

    /**
     * Add CPT to GraphPanel
     *
     * @param c CPT id
     * @return CPT that has been added
     */
    CPT addCPT(CPT c) {
        cm.registerComponent(c);
        add(c);
        return c;

    }

    /**
     * Add INFO to GraphPanel
     *
     * @param i INFO id
     * @return INFO that has been added
     */
    INFO addINFO(INFO i) {
        cm.registerComponent(i);
        add(i);
        return i;

    }

    /**
     * Draw arrow from one point to another point
     *
     * @param g1 Graphics
     * @param x1 x coordinate of start point
     * @param y1 y coordinate of start point
     * @param x2 x coordinate of end point
     * @param y2 y coordinate of end point
     */
    void drawArrow(Graphics g1, int x1, int y1, int x2, int y2) {
        Graphics2D g = (Graphics2D) g1.create();
        double dx = x2 - x1, dy = y2 - y1;
        double angle = Math.atan2(dy, dx);
        g.setColor(Color.BLACK);
        Polygon triangle = new Polygon();
        if (Math.abs(angle) <= Math.PI / 4) {
            x1 = x1 + nodeSize / 2;
            y1 = y1 + nodeSize / 2;
            x2 = x2 - nodeSize / 2;
            y2 = y2 + nodeSize / 2;
            triangle.addPoint(x2, y2);
            triangle.addPoint(x2 - nodeSize / 10, y2 - nodeSize / 10);
            triangle.addPoint(x2 - nodeSize / 10, y2 + nodeSize / 10);
            g.fillPolygon(triangle);
            g.drawLine(x1, y1, x2 - nodeSize / 10, y2);

        } else if (Math.abs(angle) <= Math.PI * 3 / 4) {
            if (angle > 0) {
                y1 = y1 + nodeSize;
                triangle.addPoint(x2, y2);
                triangle.addPoint(x2 - nodeSize / 10, y2 - nodeSize / 10);
                triangle.addPoint(x2 + nodeSize / 10, y2 - nodeSize / 10);
                g.fillPolygon(triangle);
                g.drawLine(x1, y1, x2, y2 - nodeSize / 10);

            } else {
                y2 = y2 + nodeSize;
                triangle.addPoint(x2, y2);
                triangle.addPoint(x2 - nodeSize / 10, y2 + nodeSize / 10);
                triangle.addPoint(x2 + nodeSize / 10, y2 + nodeSize / 10);
                g.fillPolygon(triangle);
                g.drawLine(x1, y1, x2, y2 + nodeSize / 10);

            }
        } else {
            x1 = x1 - nodeSize / 2;
            y1 = y1 + nodeSize / 2;
            x2 = x2 + nodeSize / 2;
            y2 = y2 + nodeSize / 2;
            triangle.addPoint(x2, y2);
            triangle.addPoint(x2 + nodeSize / 10, y2 - nodeSize / 10);
            triangle.addPoint(x2 + nodeSize / 10, y2 + nodeSize / 10);
            g.fillPolygon(triangle);
            g.drawLine(x1, y1, x2 + nodeSize / 10, y2);

        }
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setStroke(new BasicStroke(2));
        for (int i = 0; i < edges.size(); i++) {
            drawArrow(g, edges.get(i).getFrom().getTempX() + nodeSize / 2, edges.get(i).getFrom().getTempY(), edges.get(i).getTo().getTempX() + nodeSize / 2, edges.get(i).getTo().getTempY());
        }

    }

    public void redraw() {
        //need to add repaint region width and height to improve efficiency
        sp.repaint();
    }

    public void redrawCPT() {
        //need to repaint only the CPT region
        sp.repaint();
    }

    public void redrawINFO() {
        //need to repaint only the INFO region
        sp.repaint();
    }

    /**
     * Update nodes position
     */
    public void updatePosition() {
        for (int i = 0; i < edges.size(); i++) {
            edges.get(i).getFrom().setNodeX(edges.get(i).getFrom().getTempX() + nodeSize / 2);
            edges.get(i).getFrom().setNodeY(edges.get(i).getFrom().getTempY());
            edges.get(i).getTo().setNodeX(edges.get(i).getTo().getTempX() + nodeSize / 2);
            edges.get(i).getTo().setNodeY(edges.get(i).getTo().getTempY());
        }

    }

    /**
     * Get ScrollPanel where GraphPanel lays
     *
     * @return sp ScrollPanel
     */
    ScrollPanel getScrollPanel() {
        return sp;
    }

    /**
     * Get BayesianNetwork
     *
     * @return network
     */
    public BayesianNetwork getNetwork() {
        return network;
    }

    /**
     * Get max x coordinate on the GraphPanel
     *
     * @return X_MAX
     */
    public int getX_MAX() {
        return X_MAX;
    }

    /**
     * Get max y coordinate on the GraphPanel
     *
     * @return Y_MAX
     */
    public int getY_MAX() {
        return Y_MAX;
    }

    /**
     * Set max x coordinate for the GraphPanel
     *
     * @param MAX max x coordinate
     */
    public void setX_MAX(int MAX) {
        X_MAX = MAX;
    }

    /**
     * Set max y coordinate for the GraphPanel
     *
     * @param MAX max y coordinate
     */
    public void setY_MAX(int MAX) {
        Y_MAX = MAX;
    }

    /**
     * Get Node size
     *
     * @return nodeSize
     */
    public int getNodeSize() {
        return nodeSize;
    }

    ;
     
     
    public GraphPanel() {
        setLayout(null);
        setPreferredSize(new Dimension(800, 800));
    }

    /**
     * This constructor gives a GraphPanel that knows where it lays.
     *
     * @param SP ScrollPanel where GraphPanel lays
     */
    public GraphPanel(ScrollPanel SP) {
        sp = SP;
        cptMap = new HashMap();
        infoMap = new HashMap();
        idMap = new HashMap();
        cm = new ComponentMover();
        edges = new ArrayList();
        setLayout(null);
        nodeSize = 60;
        X_MAX = 800;
        Y_MAX = 800;
        setPreferredSize(new Dimension(X_MAX, Y_MAX));

    }

    /**
     * initialize graph given Network
     *
     * @param Network Network that needs to visualize
     */
    public void init(BayesianNetwork Network) {
        network = Network;
        ParentOrderLayout layout = new ParentOrderLayout(network);
        int initialX;
        int initialY = -2 * nodeSize;
        String nodeName;
        CPT cpt;
        Node n;
        INFO info;
        int maxLayerNodeNum = 0;
        for (int i = 0; i < layout.getLayers().length; i++) {
            initialY += nodeSize * 4;
            initialX = (i % 2) * nodeSize - nodeSize;
            if (layout.getLayers()[i].size() > maxLayerNodeNum) {
                maxLayerNodeNum = layout.getLayers()[i].size();
            }
            for (int j = 0; j < layout.getLayers()[i].size(); j++) {
                initialX += nodeSize * 3;
                nodeName = network.get(layout.getLayers()[i].get(j).getIdx()).getName();
                //has to create cpt first, then cptButton in node can check bound
                cpt = new CPT(this, nodeSize, layout.getLayers()[i].get(j).getIdx(), network, initialX, initialY + (j % 2) * 5);
                info = new INFO(this, nodeSize, layout.getLayers()[i].get(j).getIdx(), network, initialX, initialY + (j % 2) * 5);
                n = new Node(this, cptMap, infoMap, layout.getLayers()[i].get(j).getIdx(), nodeSize, nodeName, initialX, initialY + (j % 2) * 5);

                cptMap.put(layout.getLayers()[i].get(j).getIdx(), cpt);
                infoMap.put(layout.getLayers()[i].get(j).getIdx(), info);
                idMap.put(layout.getLayers()[i].get(j).getIdx(), n);

                addNode(n);
                addINFO(info);
                addCPT(cpt);
            }
        }

        Edge newedge;
        for (int i = 0; i < network.size(); i++) {
            for (int j = 0; j < (network.get(i).getParentOrder()).size(); j++) {
                newedge = new Edge((Node) idMap.get(network.get(i).getParentOrder().get(j)), (Node) idMap.get(i));
                edges.add(newedge);
            }

        }

        //reset graph panel size to fill in all nodes
        X_MAX = maxLayerNodeNum * (nodeSize * 4);
        Y_MAX = layout.getLayers().length * (nodeSize * 4);
        setPreferredSize(new Dimension(X_MAX, Y_MAX));
        repaint();
    }
}
