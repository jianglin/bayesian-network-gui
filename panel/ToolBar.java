
package graph.panel;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

import graph.main.Main;
import static graph.main.Main.mainSP;
import static graph.panel.GraphPanel.cptMap;
import static graph.panel.GraphPanel.infoMap;
import static graph.panel.ScrollPanel.gp;

import datastructures.BayesianNetwork;
import fileio.HuginNetStructureReader;
import graph.component.info.INFO;
import graph.component.table.CPT;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;

/*
 * Note: 
 * import com.sun.image.codec.jpeg.JPEGCodec;
 * import com.sun.image.codec.jpeg.JPEGEncodeParam;
 * import com.sun.image.codec.jpeg.JPEGImageEncoder;
 * are not recomended in online discussion
 */

/**
 *
 * @author jianglin
 */
public class ToolBar extends javax.swing.JPanel {

    JFrame f;
    BayesianNetwork network;
    static boolean cptVisible = false;
    static boolean infoVisible = false;

    /**
     * Creates new form ToolBar
     */
    public ToolBar() {
        initComponents();
    }

    public ToolBar(JFrame F) {
        f = F;
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFileChooser1 = new javax.swing.JFileChooser();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();

        jButton1.setLabel("LoadNetwork");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Reset");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Show/Hide CPTs");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("Show/Hide Info.");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton5.setText("SaveAs JPG");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton5)
                .addContainerGap(43, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2)
                    .addComponent(jButton3)
                    .addComponent(jButton4)
                    .addComponent(jButton5))
                .addContainerGap(14, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        jFileChooser1.setFileSelectionMode(JFileChooser.FILES_ONLY);
        if (this.jFileChooser1.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            try {
                String netFilename = jFileChooser1.getSelectedFile().getCanonicalPath();
                System.out.println(netFilename);

                // first, convert it to a dot file
                HuginNetStructureReader reader = new HuginNetStructureReader();
                network = reader.read(netFilename);
                System.out.println("name " + network.get(1).getName());
                System.out.println();
                f.remove(Main.mainSP);
                Main.mainSP = new ScrollPanel(network);  //initialize graph
                f.add(mainSP);
                f.add(mainSP, BorderLayout.CENTER);
                f.pack();

            } catch (Exception ex) {
                javax.swing.JOptionPane.showMessageDialog(null, "Encounterd an exception while selecting the file:\n" + ex.getMessage());
                ex.printStackTrace();
            }
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if (network != null) {
            f.remove(Main.mainSP);
            Main.mainSP = new ScrollPanel(network);  //initialize graph
            f.add(mainSP);
            f.add(mainSP, BorderLayout.CENTER);
            f.pack();
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed

        if (network != null && GraphPanel.cptMap.size() == network.size() && GraphPanel.cptMap.size() > 0) {
            if (!cptVisible) {
                for (int id = 0; id < GraphPanel.cptMap.size(); id++) {
                    ((CPT) (cptMap.get(id))).setCPTVisible(true);
                    ((CPT) (cptMap.get(id))).setVisible(true);

                }
                cptVisible = true;
                gp.redraw();
            } else {
                for (int id = 0; id < GraphPanel.cptMap.size(); id++) {
                    ((CPT) (cptMap.get(id))).setCPTVisible(false);
                    ((CPT) (cptMap.get(id))).setVisible(false);

                }
                cptVisible = false;
                gp.redraw();
            }
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        if (network != null && GraphPanel.infoMap.size() == network.size() && GraphPanel.infoMap.size() > 0) {
            if (!infoVisible) {
                for (int id = 0; id < GraphPanel.infoMap.size(); id++) {
                    ((INFO) (infoMap.get(id))).setINFOVisible(true);
                    ((INFO) (infoMap.get(id))).setVisible(true);
                }
                infoVisible = true;
                gp.redraw();
            } else {
                for (int id = 0; id < GraphPanel.infoMap.size(); id++) {
                    ((INFO) (infoMap.get(id))).setINFOVisible(false);
                    ((INFO) (infoMap.get(id))).setVisible(false);
                }
                infoVisible = false;
                gp.redraw();
            }
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed

        if (network != null) {
            final JFileChooser fc = new JFileChooser();
            fc.addChoosableFileFilter(new FileNameExtensionFilter("File JPG (.jpg)", "jpg"));
            fc.setFileFilter(fc.getChoosableFileFilters()[0]);
            File file;
            int returnVal = fc.showSaveDialog(this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {

                //      file = fc.getSelectedFile(); 
                Component components = mainSP.getScrollPane().getViewport().getView();
                Image capturedImage = components.createImage(components.getWidth(),
                        components.getHeight());
                FileOutputStream out = null;
                try {
                    file = new File(fc.getSelectedFile().getCanonicalPath() + "." + ((FileNameExtensionFilter) fc.getFileFilter()).getExtensions()[0]);
                    out = new FileOutputStream(file);
                } catch (IOException ex) {
                    Logger.getLogger(ToolBar.class.getName()).log(Level.SEVERE, null, ex);
                }

                Graphics captureG = capturedImage.getGraphics();
                components.paint(captureG);
                JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
                BufferedImage bufferedImage = (BufferedImage) capturedImage;
                JPEGEncodeParam param = encoder.getDefaultJPEGEncodeParam(bufferedImage);

                param.setQuality(1.0f, true);
                try {
                    encoder.encode(bufferedImage, param);
                    out.flush();
                    out.flush();
                } catch (IOException e) {
                }

            }
        }
    }//GEN-LAST:event_jButton5ActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JFileChooser jFileChooser1;
    // End of variables declaration//GEN-END:variables
}
