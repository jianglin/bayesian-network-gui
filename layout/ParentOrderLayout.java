package graph.layout;

import datastructures.BayesianNetwork;
import java.util.ArrayList;

/**
 * A class to represent a simple parent order layout algorithm
 *
 * @author jianglin
 */
public class ParentOrderLayout {

    /**
     * The BayesianNetwork which has stored information
     */
    BayesianNetwork network;
    /**
     * The ArrayList that stores the layer information of the nodes
     */
    ArrayList<LNode>[] layers;

    /**
     * Get Layout
     *
     * @return layers layout of the network
     */
    public ArrayList<LNode>[] getLayers() {
        return layers;
    }

    public ParentOrderLayout() {
        network = null;
        layers = null;
    }

    /**
     * This constructor gives a layout that knows Network.
     *
     * @param Network network that needs to be laid out
     */
    public ParentOrderLayout(BayesianNetwork Network) {
        network = Network;
        ArrayList<LNode> layer0 = new ArrayList();
        for (int i = 0; i < network.size(); i++) {
            layer0.add(new LNode(network.get(i).getIndex()));
        }
        boolean hasChanged = true;
        int maxLayer = 0;
        while (hasChanged) {
            hasChanged = false;
            for (int i = 0; i < network.size(); i++) {
                if (!network.get(i).getParentOrder().isEmpty()) {
                    for (int j = 0; j < network.get(i).getParentOrder().size(); j++) {
                        if (layer0.get(network.get(i).getIndex()).getLevel() <= layer0.get(network.get(i).getParentOrder().get(j)).getLevel()) {
                            layer0.get(network.get(i).getIndex()).setLevel(layer0.get(network.get(i).getParentOrder().get(j)).getLevel() + 1);
                            if (layer0.get(network.get(i).getIndex()).getLevel() > maxLayer) {
                                maxLayer = layer0.get(network.get(i).getIndex()).getLevel();
                            }
                            hasChanged = true;
                        }
                    }
                }

            }

        }


        layers = new ArrayList[maxLayer + 1];
        for (int i = 0; i < maxLayer + 1; i++) {
            layers[i] = new ArrayList();
        }
        for (int i = 0; i < network.size(); i++) {
            layers[layer0.get(network.get(i).getIndex()).getLevel()].add(layer0.get(i));
        }



    }
}
