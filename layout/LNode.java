package graph.layout;

/**
 * A class to represent a node for layout algorithm
 *
 * @author jianglin
 */
public class LNode {

    /**
     * index of layout Node
     */
    int idx;
    /**
     * layout level of the LNode
     */
    int level;

    /**
     * Get index of the Node
     *
     * @return idx of the Node
     */
    public int getIdx() {
        return idx;
    }

    /**
     * Get level of the Node
     *
     * @return level of the Node
     */
    public int getLevel() {
        return level;
    }

    /**
     * Set level of the node for layout algorithm
     *
     * @param Level get from layout algorithm
     */
    public void setLevel(int Level) {
        level = Level;
    }

    public LNode() {
        idx = -1;
        level = 0;
    }

    /**
     * This constructor gives a LNode that knows its index.
     *
     * @param n index of the LNode
     */
    public LNode(int n) {
        idx = n;
        level = 0;
    }
}
